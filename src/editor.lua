local S = core.get_translator("gems_battle")
arena_lib.register_editor_section("gems_battle", {

    name = "gems_battle_settings",
    icon = "gems_editor.png",
    hotbar_message = S("Gems Battle Arena Settings"),
    give_items = function(itemstack, user, arena)

        for _,team_name in pairs(gems_battle.team_names) do
            -- show shop markers
            if arena[team_name.."_shop"] then
                local staticdata = core.write_json({
                    _arena = arena.name,
                    _property = team_name.."_shop",
                    _textures = "gems_storemarker_"..team_name..".png"
                })
                local obj = core.add_entity(arena[team_name.."_shop"], "gems_battle:pos_ent", staticdata)
            end
            -- show great gem markers
            if arena[team_name.."_gem"] then
                local staticdata = core.write_json({
                    _arena = arena.name,
                    _property = team_name.."_gem",
                    _textures = "gems_goalmarker_"..team_name..".png"
                })
                local obj = core.add_entity(arena[team_name.."_gem"], "gems_battle:pos_ent", staticdata)
            end

        end

        if arena.jail then
            local staticdata = core.write_json({
                _arena = arena.name,
                _property = "jail",
                _textures = "gems_jail.png"
            })
            local obj = core.add_entity(arena.jail, "gems_battle:pos_ent", staticdata)
        end



        local tbl = {"gems_battle:goals","gems_battle:shops","gems_battle:mines","gems_battle:jail"}
        return tbl
    end,
})


local function on_use_give_tools(item_name,itemstack, user, pointed_thing)
    local mod = user:get_meta():get_string("arena_lib_editor.mod")
    local arena_name = user:get_meta():get_string("arena_lib_editor.arena")
    local id, arena = arena_lib.get_arena_by_name(mod, arena_name)
    local tools = {"","","","","","","","","arena_lib:editor_return"}
    for i,stats in ipairs(arena.teams) do
        tools[i] = item_name..stats.name
    end
    user:get_inventory():set_list("main", tools)
end

core.register_tool("gems_battle:goals",{
    description = S("Edit Great Gem Locations"),
    inventory_image = "gems_goalmarker_ruby.png",
    wield_image = "gems_goalmarker_ruby.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        on_use_give_tools("gems_battle:goal_", itemstack, user, pointed_thing)
    end
})


local mines_tool_image = "default_stone.png^item_ruby.png"
core.register_tool("gems_battle:mines",{
    description = S("Place Mines"),
    inventory_image = mines_tool_image,
    wield_image = mines_tool_image,
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        on_use_give_tools("gems_battle:mine_", itemstack, user, pointed_thing)
    end
})



core.register_tool("gems_battle:shops",{

    description = S("Edit Shop Locations"),
    inventory_image = "gems_storemarker_ruby.png",
    wield_image = "gems_storemarker_ruby.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,

    on_use = function(itemstack, user, pointed_thing)
        on_use_give_tools("gems_battle:shop_", itemstack, user, pointed_thing)
    end
})

-- property: one of: "gem", "shop"
local function on_use(team_name,capname,property,texturestring,itemstack, user, pointed_thing)
    local mod = user:get_meta():get_string("arena_lib_editor.mod")
    local arena_name = user:get_meta():get_string("arena_lib_editor.arena")
    local pos = vector.round(user:get_pos())
    local pos_string = dump(pos)
    arena_lib.change_arena_property(user:get_player_name(), 'gems_battle', arena_name, team_name.."_"..property, pos_string , true)
    core.chat_send_player(user:get_player_name(), capname.." "..property.." has been set to:"..vector.to_string(pos)) 
    local staticdata = core.write_json({
        _arena = arena_name,
        _property = team_name.."_"..property,
        _textures = texturestring
    })
    core.add_entity(pos, "gems_battle:pos_ent", staticdata)
end


for _,team_name in pairs(gems_battle.team_names) do
    local capname = gems_battle.get_capname(team_name)
    local goal_texture_string = "gems_goalmarker_"..team_name..".png"
    local shop_texture_string = "gems_storemarker_"..team_name..".png"

    core.register_tool("gems_battle:goal_"..team_name,{

        description = S("@1 Great Gem",capname),
        inventory_image = goal_texture_string,
        wield_image = goal_texture_string,
        groups = {not_in_creative_inventory = 1},
        on_place = function() end,
        on_drop = function() end,
        on_use = function(itemstack, user, pointed_thing)
            on_use(team_name,capname,"gem",goal_texture_string,itemstack, user, pointed_thing)
        end
    })
    core.register_tool("gems_battle:shop_"..team_name,{
        description = S("@1 Shop",capname),
        inventory_image = shop_texture_string,
        wield_image = shop_texture_string,
        groups = {not_in_creative_inventory = 1},
        on_place = function() end,
        on_drop = function() end,
        on_use = function(itemstack, user, pointed_thing)
            on_use(team_name,capname,"shop",shop_texture_string,itemstack, user, pointed_thing)
        end
    })
end

core.register_tool("gems_battle:jail",{
    description = S("Jail Spawn"),
    inventory_image = "gems_jail.png",
    wield_image = "gems_jail.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local mod = user:get_meta():get_string("arena_lib_editor.mod")
        local arena_name = user:get_meta():get_string("arena_lib_editor.arena")
        local pos = vector.round(user:get_pos())
        local pos_string = dump(pos)
        arena_lib.change_arena_property(user:get_player_name(), 'gems_battle', arena_name, "jail", pos_string , true)
        core.chat_send_player(user:get_player_name(), S("Jail has been set to: @1",vector.to_string(pos)))
        local staticdata = core.write_json({
            _arena = arena_name,
            _property = "jail",
            _textures = "gems_jail.png"
        })
        core.add_entity(pos, "gems_battle:pos_ent", staticdata)
    end
})