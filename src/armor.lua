local S = core.get_translator("gems_battle")
local gem_names = gems_battle.team_names
local gem_names_caps = gems_battle.team_names_caps

if core.get_modpath("3d_armor") then

    for _ , gem_name in pairs(gem_names) do

        local gem_name_cap = gem_names_caps[_]
        armor:register_armor("gems_battle:shield_"..gem_name, {
            description = S("@1 Shield",gem_name_cap),
            inventory_image = "gems_inv_shield_"..gem_name..".png",
            groups = {armor_shield=1, armor_heal=7, armor_use=200,not_in_creative_inventory = 1},
            armor_groups = {fleshy=15},
            damage_groups = {cracky=2, snappy=1, choppy=1, level=3},
            on_damage = function(player, index, stack)
                --play_sound_effect(player, "default_glass_footstep")
            end,
            on_destroy = function(player, index, stack)
                --play_sound_effect(player, "default_break_glass")
            end,
            on_use = function(itemstack, user, pointed_thing)
                if user and user:is_player() then
                    local weared_elems = armor:get_weared_armor_elements(user)
                    if weared_elems["shield"] then
                        armor:unequip (user, "shield")
                    end
                    local itemstack = armor:equip (user, itemstack)
                    return itemstack
                end
            end,
        })

    end

end

-- detect the version of 3d armor used (mod or modpack)
local steel_armor = "3d_armor"


local armors = {"leggings", "helmet", "chestplate", "boots"}
for _, armor_thing in pairs(armors) do
    local item_name_to_copy = "3d_armor:"..armor_thing.."_steel"
    local gems_itemname = "gems_battle:"..armor_thing.."_steel"
    local def = table.copy(core.registered_items[item_name_to_copy])
    def.on_use = function(itemstack,user,pointed_thing)
        local weared_elems = armor:get_weared_armor_elements(user)
        local item_element = armor:get_element(itemstack:get_name())
        if weared_elems[item_element] then
            armor:unequip(user, item_element)
        end
        armor:equip(user, itemstack)
        itemstack:take_item()
        return itemstack
    end
    armor:register_armor(gems_itemname, def)
end
