local S = core.get_translator("gems_battle")
local gems_huds = {}
local function get_great_gem_images(arena,team) end
function gems_battle.add_hud(arena,p_name,as_spectator)
    gems_huds[p_name]={}
    local player = core.get_player_by_name(p_name)
    if not player then return end 
    local teams = gems_battle.get_active_team_names(arena)
    
    for i,team in ipairs(teams) do
        -- great gem icons

        local great_gem_image,great_gem_image_simple = get_great_gem_images(arena,team)

        gems_huds[p_name][team.."_great_gem_icon"] = player:hud_add({
                hud_elem_type   = "image",
                number          = 0xE6482E,
                position        = { x = 0, y = .9},
                offset          = {x = 3*18*(i+.5),   y = -148},
                text            = great_gem_image,
                alignment       = {x = 1, y = 1},
                scale           = {x = 3, y = 3},
                z_index         = 200
        })

        -- player counter

        local players = arena_lib.get_players_in_team(arena, gems_battle.get_team_id_by_name(arena,team))
        gems_huds[p_name][team.."_player_counter"] = player:hud_add({
                hud_elem_type   = "text",
                number          = tonumber("0x"..string.sub(gems_battle.get_color_code(team,true),2,7)),
                position        = { x = 0, y = .9},
                offset          = {x = 3*18*(i+.5)+20,   y = -48},
                text            = tostring(#players),
                alignment       = {x = 1, y = 1},
                scale           = {x = 16, y = 16},
                z_index         = 200
        })

        -- gem icon

        local image = "item_"..team..".png"
        gems_huds[p_name][team.."_gem_icon"] = player:hud_add({
                hud_elem_type   = "image",
                number          = 0xE6482E,
                position        = { x = 0, y = .9},
                offset          = {x = 3*18*(i+.5),   y = -24},
                text            = image,
                alignment       = {x = 1, y = 1},
                scale           = {x = 3, y = 3},
                z_index         = 200
        })

        -- you indicator
        if not as_spectator then
            local image = "item_"..team..".png"
            if arena.players[p_name].teamID == gems_battle.get_team_id_by_name(arena,team) then 
                gems_huds[p_name].you = player:hud_add({
                    hud_elem_type   = "image",
                    number          = 0xE6482E,
                    position        = { x = 0, y = .9},
                    offset          = {x = 3*18*(i+.5),   y = -180},
                    text            = "gems_you.png",
                    alignment       = {x = 1, y = 1},
                    scale           = {x = 3, y = 3},
                    z_index         = 200
                })
            end
        end


        -- gem location waypoints
        gems_huds[p_name][team .. "_gem_location_waypoints"] = player:hud_add({
            hud_elem_type = "image_waypoint",
            text = great_gem_image_simple,
            world_pos = vector.add(arena[team.."_gem"],vector.new(0,3,0)), -- World position of the waypoint.
            scale = {x = 2, y = 2},
            alignment = 0,
            z_index = 3,
            offset = {x = 0, y = -20},
        })


        -- shop indicators
        gems_huds[p_name][team .. "_store_location_waypoints"] = player:hud_add({
            hud_elem_type = "image_waypoint",
            text = "gems_storemarker_"..team ..".png", -- The name of the texture to display.
            world_pos = vector.add(arena[team.."_shop"],vector.new(0,2.5,0)), -- World position of the waypoint.
            scale = {x = 2, y = 2},
            alignment = 0,
            z_index = 3,
            offset = {x = 0, y = -20},
        })



    end
    -- game timers
    gems_huds[p_name]["background_timer"] = player:hud_add({
        hud_elem_type = "image",
        number        = 0xE6482E,
        position  = {x =  0.5, y = 0},
        offset = {x = 0, y = 50},
        text      = "gems_timer.png",

        alignment = {x = 0, y=0},
        scale     = {x = 3.5, y = 3.5},
        z_index = 4
    })
    gems_huds[p_name]["timer"] = player:hud_add({
        hud_elem_type = "text",
        position  = {x = 0.5, y = 0},
        offset = {x = -20 , y = 50+3},
        text      = "",
        alignment = {x = 0, y=0},
        scale     = {x = 100, y = 100},
        number    = 0xdff6f5,
        z_index = 5
    })
end

function gems_battle.update_hud(arena,p_name,as_spectator)
    if not arena_lib.is_player_in_arena(p_name, "gems_battle") then return end
    local teams = {}
    for id,team in ipairs(arena.teams) do
        table.insert(teams,team.name)
    end
    local player = core.get_player_by_name(p_name)
    if not player then return end 

    local p_huds = gems_huds[p_name]

    -- handle timer HUD
    player:hud_change(p_huds.timer, "text", arena.current_time)
    -- great gem
    for t_id,teamdata in ipairs(arena.teams) do

        local team = teamdata.name
        local great_gem_image,great_gem_image_simple = get_great_gem_images(arena,team)

        player:hud_change(p_huds[team.."_great_gem_icon"], "text", great_gem_image)

        -- players counter

        local players = arena_lib.get_players_in_team(arena, gems_battle.get_team_id_by_name(arena,team))        
        if players == nil then players = {} end
        player:hud_change(p_huds[team.."_player_counter"], "text", tostring(#players))


        -- gem location waypoints
        if teamdata.gem_exists then
            player:hud_change(p_huds[team .. "_gem_location_waypoints"], "text", great_gem_image_simple)

        -- get rid of the gems hud waypoints that no longer exist
        elseif gems_huds[p_name][teamdata.name .. "_gem_location_waypoints"] then
            player:hud_remove(p_huds[teamdata.name .. "_gem_location_waypoints"])
            p_huds[teamdata.name .. "_gem_location_waypoints"]=nil
        end
    end
    if not as_spectator then
        -- handle jail
        local stats = arena.players[p_name]
        if stats and stats.in_jail then
            local time_left = math.round(stats.jail_time,1)
            if not p_huds.jail then
                p_huds.jail = player:hud_add({
                    hud_elem_type   = "image",
                    position        = { x = .5, y = .6},
                    offset          = {x = 0,   y = 0},
                    text            = "gems_jail.png",
                    alignment       = {x = 0, y = 0},
                    scale           = {x = 16, y = 16},
                    z_index         = 200
                })
                arena_lib.HUD_send_msg("hotbar", p_name, S("In Jail: @1", time_left))
            else
                arena_lib.HUD_send_msg("hotbar", p_name, S("In Jail: @1", time_left))
            end
        elseif p_huds.jail then
            player:hud_remove(p_huds.jail)
            p_huds.jail=nil
            arena_lib.HUD_hide("hotbar", p_name)
        end
    end

end

function gems_battle.remove_hud(arena,p_name)
    local player = core.get_player_by_name(p_name)
    if not player then return end
    if not gems_huds[p_name] then return end

    for name,id in pairs(gems_huds[p_name]) do
        -- core.debug(name)
        player:hud_remove(id)
    end
end

function get_great_gem_images(arena,team)

    local team_id = gems_battle.get_team_id_by_name(arena, team)
    local is_active = arena.teams[team_id].gem_exists
    local percent = ((arena.teams[team_id].gem_hp/32))*100

    local color = gems_battle.get_color_code(team)

    local great_gem_border = "gems_hud_border.png"
    local great_gem_border_simple = "gems_hud_border_plain.png"
    if not(is_active) then
        great_gem_border = "gems_hud_border_elim.png"
    end

    if arena.teams[team_id].attack_flash then
        great_gem_border = "gems_hud_border_attack.png"
        great_gem_border_simple = "gems_hud_border_plain_attack.png"
    end

    local great_gem_image = [[[combine:16x40:0,0=]]..great_gem_border..[[:4,1=gems_hud_overlay.png\^[lowpart\:]]..percent..[[\:gems_hud_overlay.png\\^[colorize\\:]]..color..[[\\:130]]
    local great_gem_image_simple = [[[combine:16x40:0,0=]]..great_gem_border_simple..[[:4,1=gems_hud_overlay.png\^[lowpart\:]]..percent..[[\:gems_hud_overlay.png\\^[colorize\\:]]..color..[[\\:130]]

    if arena.teams[team_id].gem_exists == false then 
        great_gem_image = [[[combine:16x40:0,0=gems_hud_border.png:4,1=gems_hud_overlay.png]]
        great_gem_image_simple = [[[combine:16x40:0,0=gems_hud_border_plain.png:4,1=gems_hud_overlay.png]]
    end

    if not(is_active) then
        great_gem_image_simple = "blank.png"
    end

    return great_gem_image,great_gem_image_simple
end