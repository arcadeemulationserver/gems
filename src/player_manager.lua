core.register_on_player_hpchange(function(player,hp_change,reason)
    local p_name = player:get_player_name()
    if arena_lib.is_player_playing(p_name, 'gems_battle') then
        -- core.debug("1")
        local arena = arena_lib.get_arena_by_player(p_name)
        local hp = player:get_hp()
        --exclude some unwanted possibilities
        if arena.in_celebration then --protect winners from damage
            return 0
        end
        -- core.debug("2")
        if hp + hp_change <= 0 then --dont ever kill players, but if a damage *would* kill them, then eliminate them, and set their health back to normal
            gems_battle.on_death(arena, p_name, reason)
            return -1
        else
            return hp_change --if it would not kill players then apply damage as normal
        end
    else
        return hp_change
    end
end, true)